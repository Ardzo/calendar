# Version history:

## **1.2** 2021-04-06
* Code is aligned to PSR. DocBlock comments is added.
* Namespace is added.
* Functions and some variables is renamed.
* Variable "$month_names" is abolished.
* Initializing of "$time_var" is fixed.
* Empty day block - name of variable is fixed.
* Template for past days is added.
* Control of unavailable dates is added: list, template.
* Other minor changes

## **1.1** 2004-07-02
* New functions get_url_other_year(), get_url_other_month(), date().
* Changes in processing of template and template itself.

## **1.0** 2003-11-20
* This is first release.
