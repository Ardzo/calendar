# Library for output of calendar

## Introduction
This is class (on PHP) that allow to show a calendar and operate with
it. Builded on templates, flexible and easy to tune.

## Requirements

* PHP 5.0+
* Windows or Unix

## Using
Copy "Calendar.php" from "src" onto server and use it in your script.
Or use Composer - class will loads automatically.
See example code in folder "example".
