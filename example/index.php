<?php
require 'Calendar.php';

$calendar_1 = new Ardzo\Calendar\Calendar;
$calendar_1->tmpl_day_past = '
	$this->day = "
		<td align=\"right\">$eval_day</td>
	";
';
$calendar_1->tmpl_day_unavailable = '
	$this->day = "
		<td align=\"right\" style=\"color:red\">$eval_day</td>
	";
';
$calendar_1->tmpl_day_future = '
	$this->day = "
		<td align=\"right\"><a href=\"#\">$eval_day</a></td>
	";
';
$calendar_1->tmpl_day_current = '
	$this->day = "
		<td align=\"right\"><a href=\"#\"><strong>$eval_day</strong></a></td>
	";
';
$calendar_1->dayempty = '<td>&nbsp;</td>';
$calendar_1->week_delimiter = '</tr><tr>';
$calendar_1->tmpl_url = '?time=[TIME]';
$calendar_1->unavailable_dates = array('2021-04-01', '2021-04-09');

$calendar_time = strtotime(date('Y').'-'.date('m').'-01');

echo '
	'.date('F', $calendar_time).'
	<p />
	<table>
	<tr>
	'.$calendar_1->showCalendar().'
	</tr>
	</table>
';
?>
