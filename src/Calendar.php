<?php
/**
* Library for output of calendar
*
* @version 1.2
* @copyright Copyright (C) 2003-2021 Richter
* @author Richter (richter@wpdom.com)
* @link http://wpdom.com
*/

namespace Ardzo\Calendar;

class Calendar {
    public $time_var;             // Name of variable in which sent time
    public $tmpl_day_past;        // Template of regular day in past
    public $tmpl_day_future;      // Template of regular day in future
    public $tmpl_day_current;     // Template of current day
    public $tmpl_day_unavailable; // Template of day unavailable to interact
    public $dayempty;             // Empty day block (stub)
    public $week_delimiter;       // Block for week delimiter
    public $tmpl_url;             // URL
    public $unavailable_dates;    // List (array) with dates unavailable to interact

    function __construct()
    {
        $this->tmpl_url = '[TIME]';
        $this->time_var = 'calendar_time';
    }

    /** Shows a calendar */
    public function showCalendar()
    {
        $time = $GLOBALS[$this->time_var];
        $out  = '';
        if (isset($time)) {
            $y        = date('Y', $time);
            $tmpmonth = date('m', $time);
            $c_day    = date('d', $time);
        }
        else {
            $y        = date('Y');
            $tmpmonth = date('m');
            $c_day    = date('d');
        }
        $firstday = date('w', mktime(0, 0, 0, $tmpmonth, 1, $y));
        if ($firstday == 0) $firstday = 7;
        $lastday = date('d', mktime(0, 0, 0, $tmpmonth+1, 0, $y));

        foreach ($this->unavailable_dates as $val)
            $unavailable_datestamps[] = mktime(0, 0, 0, substr(trim($val), 5, 2), substr(trim($val), 8, 2), substr(trim($val), 0, 4));

        // Table of calendar
        $count = 0;
        $days_rows = '';
        // Adding empty blocks before first day
        for ($i=2; $i<=$firstday; $i++) {
            $days_rows .= $this->dayempty;
            $count++;
        }
        // Main days cycle
        for ($i=1; $i<=$lastday; $i++) {
            if ($count == 7) {
                $days_rows .= $this->week_delimiter;
                $count = 0;
            }
            $count++;
            $tmpday = date('d', mktime(0, 0, 0, $tmpmonth, $i, $y));
            // Links to Jump in "days"
            $eval_url   = str_replace('[TIME]', mktime(0, 0, 0, $tmpmonth, $tmpday, $y), $this->tmpl_url);
            $eval_day   = $i;
            $eval_month = $tmpmonth;
            $eval_year  = $y;
            if (in_array(mktime(0, 0, 0, $tmpmonth, $i, $y), $unavailable_datestamps)) {
                eval($this->tmpl_day_unavailable);
            } elseif (mktime(0, 0, 0, $tmpmonth, $i, $y) < mktime(0, 0, 0, date('m'), date('d'), date('Y'))) {
                eval($this->tmpl_day_past);
            } elseif (mktime(0, 0, 0, $tmpmonth, $i, $y) == mktime(0, 0, 0, date('m'), date('d'), date('Y'))) { // "Today"
                eval($this->tmpl_day_current);
            } else { // Other days
                eval($this->tmpl_day_future);
            }
            $days_rows .= $this->day;
        }
        // Adding empty blocks after last day
        if ($count<7) {
            $i = 1;
            while ($count<7) {
                $days_rows .= $this->dayempty;
                $count++;
                $i++;
            }
        }
        return $days_rows;
    }

    /** Get URL for other month
    * @param int $shift can be +-1
    */
    public function getOtherMonthUrl($start_timestamp, $shift)
    {
        if (!$start_timestamp) $start_timestamp = time();
        $tmp_month = date('m', $start_timestamp);
        if ($shift == '-1') {
            if ($tmp_month == '01') $time = mktime(0, 0, 0, 12, date('d', $start_timestamp), date('Y', $start_timestamp)-1);
            else $time = mktime(0, 0, 0, $tmp_month-1, date('d', $start_timestamp), date('Y', $start_timestamp));
        } else {
            if ($tmp_month == '12') $time = mktime(0, 0, 0, 1, date('d', $start_timestamp), date('Y', $start_timestamp)+1);
            else $time = mktime(0, 0, 0, $tmp_month+1, date('d', $start_timestamp), date('Y', $start_timestamp));
        }
        return str_replace('[TIME]', $time, $this->tmpl_url);
    }

    /** Get URL for other year */
    public function getOtherYearUrl($start_timestamp, $shift)
    {
        if (!$start_timestamp) $start_timestamp = time();
        return str_replace('[TIME]', mktime(0, 0, 0, date('m', $start_timestamp), date('d', $start_timestamp), date('Y', $start_timestamp)+$shift), $this->tmpl_url);
    }

    /** Get normal date from timestamp (analogue of standard date()) */
    public function date($format, $timestamp)
    {
        if (!$timestamp) $timestamp = time();
        return date($format, $timestamp);
    }
}
?>
